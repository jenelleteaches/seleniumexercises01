import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Example01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("HELLO WORLD!");
		
		// windows
		// System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");

		// mac
		System.setProperty("webdriver.chrome.driver","/Users/macstudent/Desktop/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		
		// A) SET EXPECTED RESULT
		String expectedTitle = "Welcome: Mercury Tours";
		
		// B) GO AND GET THE ACTUAL RESULT
		
		// 1. launch Chrome and direct it to the Base URL
//		String baseUrl = "http://demo.guru99.com/test/newtours/";
//		driver.get(baseUrl);
//		
		
		driver.get("http://www.google.com");
		driver.findElement(By.name("q")).sendKeys("sacred games episodes");
		driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[2]/div[2]/div/center/input[1]")).click();
		
		boolean hasHotelsKeyword = driver.getPageSource().contains("APPLE");
		
		if (hasHotelsKeyword == true) {
			System.out.println("TEST PASSED");
		}
		else {
			System.out.println("TEST FAILED");
		}
		
		
		
		
		
		
		// 2. get the title of the webpage
		String actualTitle = driver.getTitle();
		
		/*
		 * C) COMPARE actual title of the page with the expected one and print
		 * the result as "Passed" or "Failed"
		 */
		if (actualTitle.contentEquals(expectedTitle)){
		    System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}
		   
	    // D) close Chrome
	    driver.close();
		
		
	}

}
